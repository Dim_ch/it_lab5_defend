﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using lab5_defend;

namespace lab5_defend.Controllers
{
    public class HospitalsController : Controller
    {
        private readonly DbAppContext _context;

        public HospitalsController(DbAppContext context)
        {
            _context = context;
        }

        // GET: Hospitals
        public async Task<IActionResult> Index()
        {
            return View(await _context.Hospitals.ToListAsync());
        }

        // GET: Hospitals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospital = await _context.Hospitals
                .Include(e => e.HospitalEquipment)
                    .ThenInclude(e => e.Equipment)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hospital == null)
            {
                return NotFound();
            }

            return View(hospital);
        }

        // GET: Hospitals/Create
        public async Task<IActionResult> Create()
        {
            var equip = await _context.Equipments.ToListAsync();
            if (equip == null)
            {
                return NotFound();
            }

            ViewBag.equip = equip;

            return View();
        }

        // POST: Hospitals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Hospital hospital, int[] selectedEquipment)
        {
            if (ModelState.IsValid)
            {
                if (selectedEquipment != null)
                {
                    foreach(var equip in _context.Equipments.Where(item => selectedEquipment.Contains(item.Id)))
                    {
                        hospital.HospitalEquipment.Add(new HospitalEquipment { HospitalId = hospital.Id, EquipmentId = equip.Id});
                    }
                }

                _context.Add(hospital);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hospital);
        }

        // GET: Hospitals/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospital = await _context.Hospitals
                    .Include(h => h.HospitalEquipment)
                    .FirstOrDefaultAsync(h => h.Id == id);
            if (hospital == null)
            {
                return NotFound();
            }

            List<int> equipment = new List<int>();
            foreach(HospitalEquipment item in hospital.HospitalEquipment)
            {
                equipment.Add(item.EquipmentId);
            }
            ViewBag.HospitalEquip = equipment;

            var equip = await _context.Equipments.ToListAsync();
            if (equip == null)
            {
                return NotFound();
            }

            ViewBag.equip = equip;

            return View(hospital);
        }

        // POST: Hospitals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Hospital hospital, List<int> selectedEquipment)
        {

            var hospitalToUpdate = await _context.Hospitals
                    .Include(h => h.HospitalEquipment)
                    .FirstOrDefaultAsync(h => h.Id == hospital.Id);

            if (hospitalToUpdate == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    hospitalToUpdate.Name = hospital.Name;
                    UpdateHospitalEquipment(hospitalToUpdate, selectedEquipment);

                    _context.Update(hospitalToUpdate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HospitalExists(hospital.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hospital);
        }

        private void UpdateHospitalEquipment(Hospital hospToUp, List<int> selectItem)
        {
            List<int> equipment = new List<int>();
            // Получаем оборудование больницы, которое выделил пользователь
            foreach (HospitalEquipment item in hospToUp.HospitalEquipment)
            {
                equipment.Add(item.EquipmentId);
            }

            if (selectItem == null)
            {
                hospToUp.HospitalEquipment.Clear();
                return;
            }

            foreach (var item in _context.Equipments)
            {
                //Оборудование выделено
                if (selectItem.Contains(item.Id))
                {
                    //Отсутствует у больницы
                    if (!equipment.Contains(item.Id))
                    {
                        hospToUp.HospitalEquipment.Add(new HospitalEquipment { EquipmentId = item.Id, HospitalId = hospToUp.Id });
                    }
                }
                // Оборудование не выделено
                else
                {
                    foreach (HospitalEquipment hospEquipment in hospToUp.HospitalEquipment)
                    {
                        // Если оборудование убрали из оборудования больницы
                        if (hospEquipment.EquipmentId == item.Id)
                        {
                            hospToUp.HospitalEquipment.Remove(hospEquipment);
                            break;
                        }
                    }
                }
            }
        }

        // GET: Hospitals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hospital = await _context.Hospitals
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hospital == null)
            {
                return NotFound();
            }

            return View(hospital);
        }

        // POST: Hospitals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hospital = await _context.Hospitals.FindAsync(id);
            _context.Hospitals.Remove(hospital);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HospitalExists(int id)
        {
            return _context.Hospitals.Any(e => e.Id == id);
        }
    }
}
