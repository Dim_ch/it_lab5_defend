﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using lab5_defend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace lab5_defend.Controllers
{
    public class TestsController : Controller
    {
        private readonly DbAppContext _context;

        private List<PatientInf> GetPatientInf()
        {
            List<PatientInf> info = new List<PatientInf>();

            foreach (var item in _context.Patients)
            {
                info.Add(new PatientInf(item));
            }

            return info;
        }

        public TestsController(DbAppContext context)
        {
            _context = context;
        }

        // GET: Tests
        public async Task<IActionResult> Index()
        {
            var dbAppContext = _context.Tests.Include(t => t.Patient);
            return View(await dbAppContext.ToListAsync());
        }

        // GET: Tests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tests = await _context.Tests
                .Include(t => t.Patient)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tests == null)
            {
                return NotFound();
            }

            return View(tests);
        }

        // GET: Tests/Create
        public IActionResult Create()
        {
            ViewData["PatientId"] = new SelectList(GetPatientInf(), "Id", "FullName");
            return View();
        }

        // POST: Tests/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Day,Patient_test,PatientId")] Tests tests)
        {
            if (ModelState.IsValid)
            {
                UpdatePatient(tests);
                _context.Add(tests);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientId"] = new SelectList(GetPatientInf(), "Id", "FullName", tests.PatientId);
            return View(tests);
        }

        // GET: Tests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tests = await _context.Tests.FindAsync(id);
            if (tests == null)
            {
                return NotFound();
            }
            ViewData["PatientId"] = new SelectList(GetPatientInf(), "Id", "FullName", tests.PatientId);
            return View(tests);
        }

        // POST: Tests/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Day,Patient_test,PatientId")] Tests tests)
        {
            if (id != tests.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tests);
                    UpdatePatient(tests);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TestsExists(tests.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientId"] = new SelectList(GetPatientInf(), "Id", "FullName", tests.PatientId);
            return View(tests);
        }

        // GET: Tests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tests = await _context.Tests
                .Include(t => t.Patient)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tests == null)
            {
                return NotFound();
            }

            return View(tests);
        }

        // POST: Tests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tests = await _context.Tests.FindAsync(id);
            _context.Tests.Remove(tests);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TestsExists(int id)
        {
            return _context.Tests.Any(e => e.Id == id);
        }

        private void UpdatePatient(Tests test)
        {
            Patient patient = _context.Patients.FirstOrDefault(p => p.Id == test.PatientId);
            if (patient != null)
            {
                patient.Infected = test.Patient_test;
                _context.Update(patient);
            }
        }
    }
}
