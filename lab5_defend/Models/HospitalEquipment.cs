﻿namespace lab5_defend
{
    public class HospitalEquipment
    {
        public int Id { get; set; }

        public int EquipmentId { get; set; }
        public Equipment Equipment { get; set; }

        public int HospitalId { get; set; }
        public Hospital Hospital { get; set; }
    }
}
