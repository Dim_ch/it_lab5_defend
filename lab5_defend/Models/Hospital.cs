﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab5_defend
{
    public class Hospital
    {
        public int Id { get; set; }
        
        [Required(ErrorMessage = "Поле название обязательно для заполнения")]
        [Display(Name = "Название")]
        public string Name { get; set; }
        
        public List<Patient> Patients { get; set; }
        public List<Doctor> Doctors { get; set; }
        [Display(Name = "Оборудование")]
        public List<HospitalEquipment> HospitalEquipment{ get; set; }

        public Hospital()
        {
            HospitalEquipment = new List<HospitalEquipment>();
        }
    }
}
