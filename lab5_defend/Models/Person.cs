﻿using System.ComponentModel.DataAnnotations;

namespace lab5_defend
{
    public abstract class Person
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле Имя обязательно для заполнения")]
        [Display(Name = "Имя")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Поле Фамилия обязательно для заполнения")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }
        [Display(Name = "Возраст")]
        [Required(ErrorMessage = "Поле Возраст обязательно для заполнения")]
        public int Age { get; set; }
    }
}
