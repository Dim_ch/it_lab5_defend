﻿using System.ComponentModel.DataAnnotations;

namespace lab5_defend
{
    public class Patient : Person
    {
        public Patient() { }

        [Display(Name = "Палата")]
        public string Room { get; set; }

        [Display(Name = "Заражён")]
        public bool Infected { get; set; }

        [Display(Name = "Больница")]
        [Required(ErrorMessage = "Поле Больница обязательно для заполнения")]
        public int? HospitalId { get; set; }
        [Display(Name = "Больница")]
        public Hospital Hospital { get; set; }

        public Tests Tests { get; set; }
    }
}
