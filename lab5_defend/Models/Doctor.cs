﻿using System.ComponentModel.DataAnnotations;

namespace lab5_defend
{
    public class Doctor : Person
    {
        public Doctor() { }

        [Display(Name = "Должность")]
        public string Position { get; set; }

        [Display(Name = "Больница")]
        public int? HospitalId { get; set; }
        [Display(Name = "Больница")]
        public Hospital Hospital { get; set; }
    }
}
