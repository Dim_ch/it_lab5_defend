﻿using Microsoft.EntityFrameworkCore;

namespace lab5_defend
{
    public class DbAppContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<Tests> Tests { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        
        public DbAppContext(DbContextOptions<DbAppContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
