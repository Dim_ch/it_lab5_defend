﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace lab5_defend.Models
{
    public class PatientInf
    {
        public int Id { get; set; }
        public string FullName { get; set; }


        public PatientInf(Patient patient)
        {
            Id = patient.Id;
            FullName = patient.Surname + " " + patient.Name + " " + patient.Patronymic;
        }
    }
}
