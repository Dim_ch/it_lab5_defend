﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace lab5_defend
{
    public class Equipment
    {     
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Описание")]
        public string Type { get; set; }

        public List<HospitalEquipment> HospitalEquipment { get; set; }

        public Equipment()
        {
            HospitalEquipment = new List<HospitalEquipment>();
        }
    }
}
