﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace lab5_defend
{
    [Index("PatientId", IsUnique = false, Name = "IX_Tests_PatientId")]
    public class Tests
    {
        public Tests() { }

        public int Id { get; set; }

        [Display(Name = "День")]
        [Required(ErrorMessage = "Поле День обязательно для заполнения")]
        public int Day { get; set; }
        [Display(Name = "Заражён")]
        public bool Patient_test { get; set; }

        [Display(Name = "Пацент")]
        public int PatientId { get; set; }
        [Display(Name = "Пациент")]
        public Patient Patient { get; set; }
    }
}
